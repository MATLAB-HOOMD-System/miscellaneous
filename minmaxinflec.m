function [mins,maxs,inflections]=minmaxinflec(x,y,plotflag)

if exist('plotflag','var')~=1
    plotflag=0;
end
    

df1=gradient(y,x)
df2=gradient(df1,x)
id=sign(df2)
idx=strfind(id,[-1 1])
idx2=strfind(id,[1 -1])
inflexionP=x(idx+1)
% plot(inflexionP,y(idx+1),'ks')
inflexionP2=x(idx2+1)
% plot(inflexionP2,,'ks')
inflections=[inflexionP inflexionP2; y(idx+1) y(idx2+1)]';




id=sign(df1)
id(id==0)=-1;
idx=strfind(id,[-1 1])
idx2=strfind(id,[1 -1])
inflexionP=x(idx+1)
% plot(inflexionP,y(idx+1),'kv')
inflexionP2=x(idx2+1)
% plot(inflexionP2,y(idx2+1),'k^')
mins=[inflexionP; y(idx+1)]';
maxs=[inflexionP2; y(idx2+1)]';

if plotflag==1
    if isempty(inflections)~=1
        plot(inflections(:,1),inflections(:,2),'ks');
    end
    if isempty(mins)~=1
        plot(mins(:,1),mins(:,2),'kv');
     end
    if isempty(maxs)~=1
        plot(maxs(:,1),maxs(:,2),'k^');
    end
end