function [Volume] = sphere_cap_vol(R,h)
%sphere_cap_vol calculates the volume of a spherical cap where R is the radius of hte sphere, and h is the height of the cap. See https://mathworld.wolfram.com/SphereicalCap.html

h(h<0)=0;
h(h>2*R)=2*R;
Volume=(pi/3)*h.*h.*(3*R-h);


end

