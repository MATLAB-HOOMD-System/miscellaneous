function [points]=make_sphere(N,r)
    if N==0
        points=[];
    else
        Ncount=1;
        a=pi*r*r/N;
        d=sqrt(a);
        M0=round(pi/d);
        d0=pi/M0;
        dp=a/d0;
        for m=0:M0-1
            theta=pi*(m+0.5)/M0;
            Mp=round(2*pi*sin(theta)/dp);
            for n=0:Mp-1
                phi=2*pi*n/Mp;
                points(Ncount,:)=r*[sin(theta)*cos(phi) sin(theta)*sin(phi) cos(theta)];
                Ncount=Ncount+1;
            end
        end
    end
end