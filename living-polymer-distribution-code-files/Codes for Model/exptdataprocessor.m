global M0 D RX DPmax decapmax DPexp PCLDexp wLogMexp meanDPexp DispExp
%set initial parameters
M0=100;
RX=0.1833;
D=1.01;

% Enter experimentally obtained DPn and dispersity
meanDPexp=4;
DispExp=1.37;

%estimate peak number of decappings and max DP
DPmax=round(20*M0/RX);
decapmax=10;


%Processes GPC distribution
nexp=size(DPe);
nexp_test=size(wLogMe);
if(nexp~=nexp_test)
    'STOP your two distributions have uneven chain lengths'
end
%initialize
j=1;
DPexp=0;
wLogMexp=0;
DPe(1,1)=round(DPe(1));
%DPexp is the refined one with duplicate DPs removed
DPexp(1,1)=DPe(1);
DPexp2(1,1)=DPe(1)^2;
PCLDunnorm(1,1)=wLogMe(1)/DPe(1)^2;
wLogMexp(1,1)=wLogMe(1);
for i=2:nexp;
    DPe(i)=round(DPe(i));
    if(DPe(i)==DPe(i-1));
        %remove duplicate but take aveage of two points
        PCLDunnorm(j,1)=0.5*(wLogMe(i)/DPe(i)^2+wLogMe(i-1)/DPe(i-1)^2);
        wLogMexp(j,1)=0.5*(wLogMe(i)+wLogMe(i-1));
    else
        j=j+1;
        DPexp(j,1)=DPe(i);
        DPexp2(j,1)=DPe(i)^2;
        PCLDunnorm(j,1)=wLogMe(i)/DPe(i)^2; 
        wLogMexp(j,1)=wLogMe(i);
    end
end
%normalize PCLD
PCLDexp=PCLDunnorm/sum(PCLDunnorm);
wLogMexp=wLogMexp/sum(wLogMexp);

