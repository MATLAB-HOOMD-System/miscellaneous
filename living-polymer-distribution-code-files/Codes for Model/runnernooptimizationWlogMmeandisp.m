global M0 D RX DPmax decapmax DPexp PCLDexp wLogMexp meanDPexp DispExp
%define experimental parameters of initial monomer concentration,
%deactivator concentration and initiator/CTA concentration

%Define the DP and the DP squared
DPs=zeros(DPmax,1);
DP2s=zeros(DPmax,1);
for i=1:DPmax;
    DPs(i)=i;
    DP2s(i)=(i)^2;
end
DPth=DPs;
%start of optimization routine initial guesses
kstarest=8e-4;
mudecapest=3;

decapmax=round(5*sqrt(mudecapest)+mudecapest);
%make decap max 5 standard deviations higher than mean of poisson
%distribution

x0=[kstarest mudecapest];

%search for minimum using number distribution with recommended tolerances
opts=optimset('fminsearch');
opts=optimset('TolFun',5e-3,'TolX',100.);
xout=fminsearch(@chi2wLogM,x0,opts)
%search for minimum using GPC distribution
%xout=fminsearch(chi2wLogM,x0)

%define optimized k* and mudecap
kstarout=xout(1);
mudecapout=xout(2);

%calculate distribution with optimized parameters both number and GPC distribution
PCLDth=PCLD(kstarout,mudecapout);

wlogMth=zeros(DPmax,1);
for i=1:DPmax;
    wlogMth(i)=(i)^2*PCLDth(i);
end

%normalize distribution by height
wlogMth=wlogMth/max(wlogMth);

%plot data and calculate mean and dispersity for both experiment and model
figure;
semilogx(DPs,wlogMth,'r');
hold on
semilogx(DPexp,wLogMexp/max(wLogMexp),'b')
%check that area under curve is close to 1, indicating adequate approximation to infinite sums
areaundercurve=sum(PCLDth)
meanth=sum(DPs.*PCLDth);
var=sum(DP2s.*PCLDth)-meanth^2;
Dispersityth=sum(DP2s.*PCLDth)/meanth^2;

meanexp=sum(DPexp.*PCLDexp);
Dispersityexp=sum(DPexp2.*PCLDexp)/meanexp^2;

meanexpmeanth=[meanexp meanth]
DispexpDispth=[Dispersityexp Dispersityth]


