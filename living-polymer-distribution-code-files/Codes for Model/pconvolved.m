function pconvolved=pconvolved(P1,P2,n);
%convolves P1 and P2 as pconvolved=sumj=1,n [P1(n-j)*P2(j)]
pconvolvedrun=zeros(n,1);
for i=1:n;
    for k=1:i;
        pconvolvedrun(i)=pconvolvedrun(i)+P1(i-k+1)*P2(k);
    end
end
pconvolved=pconvolvedrun;
end