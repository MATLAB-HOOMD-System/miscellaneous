function Pdecap=Pdecap(mudecap);
global M0 D RX DPmax decapmax
%calculates Poisson distribution with mean mudecap
for j=1:decapmax+1;
    %note that in here jdecap=j-1, where j is running index
    Pdecap(j)=(exp(-mudecap)*mudecap^(j-1))/factorial(j-1);
end
end