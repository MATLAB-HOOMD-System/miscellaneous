function Padd=Padd(kstar,jdecap);
%geometric distribution for number of units added per monomer 
% note because of the way jdecap is entered in PML this is actually the number of
% decappings on the previous cycle, or jdecap-1
global M0 D RX DPmax decapmax
mu_addj=(kstar * (M0/D))*(((1-(kstar * RX)/D))^(jdecap));
uj=mu_addj/(mu_addj+1);
Padd=zeros(DPmax,1);
for i=1:DPmax;
    %i added is i-1 where i is the running index
    Padd(i)=uj^(i-1)*(1-uj);
end