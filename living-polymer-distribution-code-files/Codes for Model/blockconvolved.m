%calculate number distribution from broad and narrow distribution. This assumes one distribution is broad and narrow
%can also work with block 1 as disbroad and block 2 as disnarrow or vice versa
for i=1:length(DPth);
    pdisbroad(i)=disbroad(i)/i^2;
    pdisnarrow(i)=disnarrow(i)/i^2;
end

%normalize distributions
pdisbroad=pdisbroad'/sum(pdisbroad);
pdisnarrow=pdisnarrow'/sum(pdisnarrow);

%calculate convolved pdisbroad,pdisnarrow
pconvoluted=pconvolved(pdisbroad,pdisnarrow,length(DPth));


%calculate mean dispsersity and GPC distribution and plot
meanth=sum(DPs.*pconvoluted)
var=sum(DP2s.*pconvoluted)-meanth^2;
Dispersityth=sum(DP2s.*pconvoluted)/meanth^2

wlogMblock=DP2s.*pconvoluted;

semilogx(DPth,wlogMblock)