function chi2wLogM=chi2wLogM(x)
global M0 D RX DPmax decapmax DPexp PCLDexp wLogMexp meanDPexp DispExp
kstarout=x(1);
mudecapout=x(2);

%confirm values are reasonable
if(kstarout<0);
    %reject this value and make chisquared very large
    chi2=1e20;
    chi2wLogM=chi2;
    [x chi2]
    chi2meananddisp=chi2;
elseif(mudecapout<1);
    %reject this value and make chisquared very large
    chi2=1e20;
    chi2wLogM=chi2;
    [x chi2]
    chi2meananddisp=chi2;
else

    %set up chi squared variable and maximum decap value
    decapmax=round(5*sqrt(mudecapout)+mudecapout);
    chi2=0;
        
    DPs=zeros(DPmax,1);
    DP2s=zeros(DPmax,1);
    for i=1:DPmax;
        DPs(i)=i;
        DP2s(i)=(i)^2;
    end
    
    %define length of experimental data
    nexp=length(DPexp);

    %calculate number distribution with estimated parameters and the mean, dispersity and GPC distribution
    PCLDth=PCLD(kstarout,mudecapout);
    
    meanth=sum(DPs.*PCLDth);
    var=sum(DP2s.*PCLDth)-meanth^2;
    Dispersityth=sum(DP2s.*PCLDth)/meanth^2;    
    
    wlogMth=zeros(DPmax,1);
    for i=1:DPmax;
        wlogMth(i)=(i)^2*PCLDth(i);
    end

    wlogMth=wlogMth/max(wlogMth);

    %sum discrepancy for each data point
    for j=1:nexp;
        wLogMexpj=wLogMexp(j)/max(wLogMexp);
        wlogMthj=wlogMth(DPexp(j));
        %factor in signal to noisein the chi2 so that the system does not
        %try to optimize a fit to noise too much
        chi2=chi2+(wLogMexpj-wlogMthj)^2;
    end
    chi2=chi2/sum(wlogMth.^2);
    %add the mean and dispersity discrepancies
    chi2=chi2+(meanth-meanDPexp)^2/meanDPexp^2;
    chi2=chi2+(Dispersityth-DispExp)^2/(DispExp)^2;
    chi2wLogM=chi2;
    [x chi2]

end
end