function PML=PML(kstar,jdecap);
global M0 D RX DPmax decapmax

%initialize with all chains being the 0 chain length (first entry)
PMLcurrent=zeros(DPmax,1);
PMLcurrent(1)=1;
%calculate number distribution for j decappings with convolution of Padd with prior distribution
for j=1:jdecap+1;
    %previous itterations current distribution becomes this itterations old
    %one
    PMLprior=PMLcurrent;
    Paddj=Padd(kstar,j-1);
    PMLcurrent=pconvolved(PMLprior,Paddj,DPmax);
end
PML=PMLcurrent;
end