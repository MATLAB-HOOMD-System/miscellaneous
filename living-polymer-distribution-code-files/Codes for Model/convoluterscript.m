%perform a simple convolution of two distributions disbroad and disnarrow
for i=1:length(DPth);
    pdisbroad(i)=disbroad(i)/i^2;
    pdisnarrow(i)=disnarrow(i)/i^2;
end
pdisbroad=pdisbroad'/sum(pdisbroad);
pdisnarrow=pdisnarrow'/sum(pdisnarrow);
convolute=pconvolved(pdisbroad,pdisnarrow,length(DPth));

%plot convolved distribution
semilogx(DPth,convolute)

    