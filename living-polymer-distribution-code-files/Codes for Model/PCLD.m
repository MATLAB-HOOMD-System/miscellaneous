function PCLD=PCLD(kstar,mudecap);
global M0 D RX DPmax decapmax
Pdecaps=Pdecap(mudecap);
PCLDwork=zeros(DPmax,1);
%Chain with 0 decappings is a unimer
%takes the distribution after on average j decappings and multiplies by the probability of seeing that number of decaying events
PCLDwork(1,1)=1*Pdecaps(1);
for j=2:decapmax+1;
    %calculate ML with j decapings
    PMLj=PML(kstar,j-1);
    %Add to existing one
    PCLDwork=PCLDwork+PMLj*Pdecaps(j);
end
PCLD=PCLDwork;
end