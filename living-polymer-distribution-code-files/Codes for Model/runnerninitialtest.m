global M0 D RX DPmax decapmax DPexp PCLDexp wLogMexp
%define experimental parameters of initial monomer concentration,
%deactivator concentration and initiator/CTA concentration

%Define the DP and the DP squared
DPs=zeros(DPmax,1);
DP2s=zeros(DPmax,1);
for i=1:DPmax;
    DPs(i)=i;
    DP2s(i)=(i)^2;
end
DPth=DPs;
%start of optimization routine initial guesses
kstarest=5e-1;
mudecapest=10;

%make decap max 5 standard deviations higher than mean of poisson
%distribution
% calculate distribution with estimated values of k* and mudecap
decapmax=round(5*sqrt(mudecapest)+mudecapest);
PCLDth=PCLD(kstarest,mudecapest);

%convert to GPC distribution for theoretical with kstarest and mudecapest
wlogMth=zeros(DPmax,1);
wlogMexp=zeros(length(DPexp),1);
for i=1:DPmax;
    wlogMth(i)=(i)^2*PCLDth(i);
end

%convert to GPC distribution for experimental
for i=1:length(DPexp);
    wlogMexp(i)=DPexp(i)^2*PCLDexp(i);
end

%plot data and calculate mean and variance
figure;
semilogx(DPs,wlogMth/max(wlogMth),'r');
hold on
semilogx(DPexp,wlogMexp/max(wlogMexp),'b')
areaundercurve=sum(PCLDth)
mean=sum(DPs.*PCLDth)
var=sum(DP2s.*PCLDth)-mean^2;
Dispersity=sum(DP2s.*PCLDth)/mean^2



