function [output] = isodd(val)
%Returns 1 if odd, 0 if even

output=~~rem(val,2);
end

