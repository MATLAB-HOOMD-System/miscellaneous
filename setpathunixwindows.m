function [path] = setpathunixwindows(path)

if ~isunix()
    path=strrep(path,'/mnt/Shared_Data/','S:/')
end
end