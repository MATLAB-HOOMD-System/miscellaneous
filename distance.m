function [dist]=distance(pos1,pos2)
    dist=0;
    for i=1:length(pos1)
        dist=dist+(pos1(i)-pos2(i))^2;
    end
    dist=sqrt(dist);
end