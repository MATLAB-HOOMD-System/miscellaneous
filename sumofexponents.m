function [y]=sumofexponents(params,x)

fs=abs(params(:,1)');
ts=abs(params(:,2)');



M=exp(-x'*(1./ts));
M2=repmat(fs,size(M,1),1);
y=sum(M.*M2,2);
