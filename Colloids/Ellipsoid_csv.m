clear all
xr=0.1;
yr=0.1;
zr=0.5;


[x,y,z] = ellipsoid(0,0,0,xr,yr,zr,5) 
figure
surf(x,y,z)
T = delaunay(x,y);
FV=trimesh(T,x,y,z)
A.Faces=FV.Faces;
A.Vertices=FV.Vertices;
FV=trimesh(T,x,y,abs(z))
B.Faces=FV.Faces+size(A.Vertices,1);
B.Vertices=FV.Vertices;
C.Faces=[A.Faces;B.Faces]
C.Vertices=[A.Vertices;B.Vertices]
csvwrite('ellipse.csv',C.Vertices)


[x,y,z]  = ellipsoid(0,0,0,xr,yr,zr,200) 
figure
surf(x,y,z)

T = delaunay(x,y);
FV=trimesh(T,x,y,z)
A.Faces=FV.Faces;
A.Vertices=FV.Vertices;
FV=trimesh(T,x,y,abs(z))
B.Faces=FV.Faces+size(A.Vertices,1);
B.Vertices=FV.Vertices;
C.Faces=[A.Faces;B.Faces]
C.Vertices=[A.Vertices;B.Vertices]


system('rm ellipse.obj')
obj_write(C, 'ellipse')
