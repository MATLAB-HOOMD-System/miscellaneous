function [output] = iseven(val)
%Returns 1 if even, 0 if odd

output=~rem(val,2);
end

