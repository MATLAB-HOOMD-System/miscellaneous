function[R]=CreateRotationMatrix(theta,v)
% create rotation matrix given a rotation angle and an axis vector

R=[cos(theta)+(v(1)^2)*(1-cos(theta)), v(1)*v(2)*(1-cos(theta))-v(3)*sin(theta), v(1)*v(3)*(1-cos(theta))+v(2)*sin(theta);
    v(2)*v(1)*(1-cos(theta))+v(3)*sin(theta), cos(theta)+v(2)*v(2)*(1-cos(theta)), v(2)*v(3)*(1-cos(theta))-v(1)*sin(theta);
    v(3)*v(1)*(1-cos(theta))-v(2)*sin(theta), v(3)*v(2)*(1-cos(theta))+v(1)*sin(theta), cos(theta)+v(3)*v(3)*(1-cos(theta))];
R=R/norm(R);