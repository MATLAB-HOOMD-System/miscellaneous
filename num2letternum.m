function [ouput]=num2letternum(num,type)

if ~exist('type','var')
    type=1;
elseif type<1 || type>2
    error('type specified out of bounds')
end

if type==1
    Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    ouput=Alphabet(num);
elseif type==2
    numlet=num2str(num);
    for i=1:length(numlet)
        if i==1
            ouput(i)=char(str2num(numlet(i))+64);
        else
            ouput(i)=char(str2num(numlet(i))+65);
        end
    end
end
end