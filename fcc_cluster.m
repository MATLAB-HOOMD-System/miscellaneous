function [Xcoord,Ycoord,Zcoord] = fcc_cluster(L,M,a)
%function [Xcoord,Ycoord,Zcoord] = fcc_cluster(L,M,a)
%generates coordinates of atoms inside fcc-cluster (face-centered-cubic)
%with cuboctatruncated shape (hovewer one can change from {111} to {110} truncation)
%it's arguments: L,M - parameters of cluster (L - size of the cluster,
%number of unit cells along three mutually perpendicular directions
%commensurate with lattice parameter, M - truncation degree, how many planes
%corresponding to the truncation remain in cluster)
%a - lattice parameter
%output parameters: Xcoord, Ycoord, Zcoord - coordinates of atoms
c = a; %for tetragonally distorted lattices c<>a
cnt = 0;                 %for counting
xyz = ones((4*L)^3,3);   %preallocating of memory for xyz array in order to increase speed of calculation
for i=0:L
   for j=0:L
      for k=0:L
          if(inclust(i,j,k,L,M))
              cnt = cnt + 1;
              xyz(cnt,:) = [a*i, a*j, c*k];     %atom in the origin of cubic unit cell         
          end         
          %calculating the coordinates of the atoms on faces
          if(inclust(i+0.5,j+0.5,k,L,M))
              cnt = cnt + 1;
              xyz(cnt,:) = [a*(i+0.5), a*(j+0.5), c*k];   
          end    
          if(inclust(i+0.5,j,k+0.5,L,M))
              cnt = cnt + 1;
              xyz(cnt,:) = [a*(i+0.5), a*j, c*(k+0.5)];
          end
          if(inclust(i,j+0.5,k+0.5,L,M))
              cnt = cnt + 1;
              xyz(cnt,:) = [a*i, a*(j+0.5), c*(k+0.5)];
          end          
      end    
   end    
end    
  Xcoord = xyz(1:cnt,1);
  Ycoord = xyz(1:cnt,2);
  Zcoord = xyz(1:cnt,3); 
  
function y = inclust_110(xc,yc,zc,L,M)
c = [abs(xc-0.5*L),abs(yc-0.5*L),abs(zc-0.5*L)];
y = 1;
if(c(1)>0.5*L || c(2)>0.5*L || c(3)>0.5*L)
   y = 0; 
end    
if(c(1)+c(2)>0.5*M || c(2)+c(3)>0.5*M || c(3)+c(1)>0.5*M)
   y = 0; 
end  
function y = inclust_111(xc,yc,zc,L,M)
c = [abs(xc-0.5*L),abs(yc-0.5*L),abs(zc-0.5*L)];
y = 0;
if(c(1)<=0.5*L && c(2)<=0.5*L && c(3)<=0.5*L && c(1)+c(2)+c(3)<=M) 
    y = 1;
end
function y = inclust(xc,yc,zc,L,M)
y = inclust_111(xc,yc,zc,L,M); %by default the truncation perpendicular to <111> direction is used