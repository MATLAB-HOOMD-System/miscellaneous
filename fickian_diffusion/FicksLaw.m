close all
clear all
numpart=4000;
timestep=0.01;
partradius=0.01;
    cellLim=100;
averageingwidth=3;
time=1000;
set(0, 'DefaultFigureVisible', 'off');

v = VideoWriter('test','Uncompressed AVI');
open(v);
pos=rand(numpart/2,2)*-(1-partradius);
postemp=rand(numpart/2,2)*(1-partradius);
postemp=postemp.*[ones(length(postemp),1) -1*ones(length(postemp),1)];
pos=[pos; postemp];

for i=1:numpart/2
    dismin=1;
    for j=1:length(pos)
        if i==j
            dis=1;
        else
            dis=sqrt(sum((pos(i,:)-pos(j,:)).^2));
        end
        if dis<dismin
            dismin=dis;
        end
    end
    while dismin<1.5*partradius
        i
        dismin;
        dismin=1;
        pos(i,:)=rand(1,2)*-0.98;
%         pos(i,:)
        for j=1:length(pos)
            if i==j
                dis=1;
            else
                dis=sqrt(sum((pos(i,:)-pos(j,:)).^2));
            end
            if dis<dismin
                dismin=dis;
            end
        end
    end
end

for i=numpart/2+1:length(pos)
    dismin=1;
    for j=1:length(pos)
        if i==j
            dis=1;
        else
            dis=sqrt(sum((pos(i,:)-pos(j,:)).^2));
        end
        if dis<dismin
            dismin=dis;
        end
    end
    while dismin<1.5*partradius
        i
        dismin;
        dismin=1;
        pos(i,:)=[rand(1)*(1-partradius) rand(1)*-(1-partradius)];
%         pos(i,:)
        for j=1:length(pos)
            if i==j
                dis=1;
            else
                dis=sqrt(sum((pos(i,:)-pos(j,:)).^2));
            end
            if dis<dismin
                dismin=dis;
            end
        end
    end
end
%             
% pos=[0 -0.5;0.5,-0.5];
fig1=figure;
MarkerSize=4;
p1=plot(pos(1:numpart/2,1),pos(1:numpart/2,2),'bo','MarkerFaceColor','b','MarkerSize',MarkerSize);
hold on
p2=plot(pos(numpart/2+1:numpart,1),pos(numpart/2+1:numpart,2),'ro','MarkerFaceColor','r','MarkerSize',MarkerSize);
hold off
axis([-1 1 -1 0]);
ax1=gca;

fig1.Units='inches'
fig1.Position=[2.8105 9.7053 7.4*2 4*2]
ax1.Units='inches'
ax1.Position=[1.5 0.6507 6*2 3*2]

curunits = get(gca, 'Units');
set(gca, 'Units', 'Points');
cursize = get(gca, 'Position');
set(gca, 'Units', curunits);

convert=cursize(4);
MarkerSize=partradius*convert;
set(p1,'MarkerSize',MarkerSize)
set(p2,'MarkerSize',MarkerSize)
ax1.YTick=[];
ax1.XTick=[];
ax2=axes(fig1,'Position', [0.1019 0.85 0.81 0.05])
axis([-1 1 0 1])
AX2=ylabel('Blue Point Density','Rotation',0,'Position',[-1.13 0.25 0],'Color','b')
ax3=axes(fig1,'Position', [0.1019 0.92 0.81 0.05])
axis([-1 1 0 1])
AX3=ylabel('Red Point Density','Rotation',0,'Position',[-1.13 0.25 0],'Color','r')





vel=(rand(numpart,2)-0.5)*2;
% vel=[1 0;-1 0];
posold=pos;
velold=vel;
velnew=velold;
for i=1:time
    i
    for j=1:length(posold)
        if posold(j,1)<-1+partradius
            velold(j,1)=abs(velold(j,1));
        end
        if posold(j,1)>1-partradius
            velold(j,1)=-abs(velold(j,1));
        end
        if posold(j,2)<-1+partradius
            velold(j,2)=abs(velold(j,2));
        end
        if posold(j,2)>0-partradius
            velold(j,2)=-abs(velold(j,2));
        end
    end
    velnew=velold;   
    for j=1:length(posold)
        if mod(j,100)==0
            j
        end
        for k=j+1:length(posold)
            dis=sqrt(sum((posold(j,:)-posold(k,:)).^2));
            if dis<partradius*2
                velnew(j,:)=velold(k,:);
                velnew(k,:)=velold(j,:);
                postemp1=posold(j,:)+velnew(j,:)*timestep;
                postemp2=posold(k,:)+velnew(k,:)*timestep;
                dis2=sqrt(sum((postemp1-postemp2).^2));
                if dis2<dis
                    velnew(j,:)=velold(j,:);
                    velnew(k,:)=velold(k,:);
                end
            end
        end
    end
    posnew=posold+velnew*timestep;
    posold=posnew;
    velold=velnew;
  
    p1=plot(posold(1:numpart/2,1),posold(1:numpart/2,2),'bo','MarkerFaceColor','b','MarkerSize',MarkerSize,'Parent',ax1);
    ax1.NextPlot='add'
    p2=plot(posold(numpart/2+1:numpart,1),posold(numpart/2+1:numpart,2),'ro','MarkerFaceColor','r','MarkerSize',MarkerSize,'Parent',ax1);
    ax1.NextPlot='replaceall'
    ax1.XLim=[-1 1];
    ax1.YLim=[-1 0];
    ax1.XTick=[];
    ax1.YTick=[];
    ax2.XTick=[];
    ax2.YTick=[];
    ax3.XTick=[];
    ax3.YTick=[];
    fig1.Color='w';

    

    cellWidth=2/cellLim;
    cells=-1:2/cellLim:1;

    
    vismax=1.5*numpart/(cellLim);
    colors=jet(101);
    
    for i=1:length(cells)-1
        maxlim=min([i+averageingwidth,length(cells)]);
        minlim=max([i-averageingwidth,1]);
        totalbins=maxlim-minlim+1;
        num=sum(posold(1:numpart/2,1)<cells(maxlim) & posold(1:numpart/2,1)>cells(minlim));
        num=num/totalbins;
        den=num/vismax;
        if den>1;
            den=1;
        end
        co=colors(round(den*100)+1,:);
        rectangle('Position',[cells(i) 0 cellWidth 1],'FaceColor',co,'EdgeColor',co,'LineWidth',0.001,'Parent',ax2)
        
        maxlim=min([i+averageingwidth,length(cells)]);
        minlim=max([i-averageingwidth,1]);
        totalbins=maxlim-minlim+1;
        num=sum(posold(numpart/2+1:numpart,1)<cells(maxlim) & posold(numpart/2+1:numpart,1)>cells(minlim));
        num=num/totalbins;
        den=num/vismax;
        if den>1;
            den=1;
        end
        co=colors(round(den*100)+1,:);
        rectangle('Position',[cells(i) 0 cellWidth 1],'FaceColor',co,'EdgeColor',co,'LineWidth',0.001,'Parent',ax3)
    end
        
    
%     velold
%     posold
%     dis
    pause(0.01)
    frame = getframe(gcf);
    writeVideo(v,frame);
end
close(v)
set(0, 'DefaultFigureVisible', 'on');