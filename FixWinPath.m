function [path] = FixWinPath(path)
%replace /mnt/Shared_Data/ with S:/ if the system is not unix
if ~isunix'
    path=strrep(path,'/mnt/Shared_Data/','S:/');
end