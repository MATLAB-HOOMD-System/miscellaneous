function [yourNumber] = str2num2(yourString)
% yourString = '2456.00A';
deleteMe = isletter(yourString); % Detect which ones are letters
yourString(deleteMe) = [];       % Delete the letters
yourNumber=str2num(yourString);

end